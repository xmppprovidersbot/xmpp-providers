#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
This file includes parts needed by all scripts.
"""
from typing import Any

import json
import logging
import os
from enum import Enum

GITLAB_INSTANCE_URL = "https://invent.kde.org"
MAIN_PROJECT_URL = f"{GITLAB_INSTANCE_URL}/melvo/xmpp-providers"

README_FILE_PATH = "README.md"
CONTRIBUTING_FILE_PATH = "CONTRIBUTING.md"
CRITERIA_FILE_PATH = "tools/criteria.py"
PROPERTIES_FILE_PATH = "properties.json"
PROVIDERS_FILE_PATH = "providers.json"
CLIENTS_FILE_PATH = "clients.json"

BADGE_COMMAND = "badge"
CHECK_URLS_COMMAND = "check_urls"
FILTER_COMMAND = "filter"
GITLAB_BOT_COMMAND = "gitlab_bot"
PRETTIFY_COMMAND = "prettify"
PROPERTY_MANAGER_COMMAND = "property_manager"
WEB_BOT_COMMAND = "web_bot"

JSON_INDENTATION = 4
LOG_FORMAT = "%(levelname)-8s %(module)-16s %(message)s"

log = logging.getLogger()


class Category(Enum):
    """This contains the provider categories."""

    ALL = "a"
    AUTOMATICALLY_CHOSEN = "A"
    MANUALLY_SELECTABLE = "B"
    COMPLETELY_CUSTOMIZABLE = "C"
    AUTOCOMPLETE = "D"


def create_parent_directories(file_path: str) -> None:
    """Creates all parent directories of a file.

    Parameters
    ----------
    file_path : str
        path of the file
    """

    directory_path = os.path.dirname(file_path)

    if len(directory_path) != 0:
        os.makedirs(directory_path, exist_ok=True)


def load_json_file(path: str) -> Any:
    """Loads a JSON file and converts it to a Python object.

    Parameters
    ----------
    path : str
        path of the JSON file being loaded

    Returns
    -------
    Any
        Python object if the file could be opened and converted, otherwise None
    """
    try:
        with open(path, "r") as file:
            return json.load(file)

    except json.decoder.JSONDecodeError as e:
        log.error(
            "File '%s' could not be loaded because of invalid JSON syntax: %s in line "
            "%s at column %s",
            path,
            e.msg,
            e.lineno,
            e.colno,
        )
        return None

    except OSError as e:
        log.error(f"File '{path}' could not be loaded: {e}")
        return None


def convert_dict_to_json_string(source: dict) -> str:
    """Converts a dict to a JSON string.

    Parameters
    ----------
    source : dict
        source to be converted to a JSON string

    Returns
    -------
    str
        source's data represented as JSON data
    """

    # A newline is appended because Python's JSON module does not add one.
    return json.dumps(source, indent=JSON_INDENTATION) + "\n"
